from django.conf.urls import url

from . import consumers

websocket_urlpatterns = [  # pylint: disable=invalid-name
    url(r'^ws/chat/(?P<chat_id>[^/]+)/$', consumers.ChatConsumer),
]
