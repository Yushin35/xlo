import json

from django.conf import settings
from django.db import IntegrityError

from channels.generic.websocket import AsyncWebsocketConsumer
from channels.db import database_sync_to_async
from hashids import Hashids

from xlo.adverts.models import Message
from xlo.adverts.serializers import MessageSerializer


class ChatService:

    @staticmethod
    def get_ids(chat_id, hash_salt=None):
        hashids = Hashids(hash_salt or settings.HASHID_SALT)
        (
            receiver,
            sender,
            advert
        ) = hashids.decode(chat_id)

        return {
            'receiver_id': receiver,
            'sender_id': sender,
            'advert_id': advert,
        }

    @staticmethod
    @database_sync_to_async
    def save_message(ids, message):
        messages = Message.objects.create(**ids, message=message)
        messages.save()

        return MessageSerializer(messages, many=False).data

    @staticmethod
    @database_sync_to_async
    def get_chat_messages(ids, count=25):
        messages = Message.objects.filter(**ids)[:count]
        return MessageSerializer(messages, many=True).data

    @staticmethod
    def create_message_meta(msgs, first_receive=False):
        return {
            'msgs': msgs,
            'meta': {
                'first_receive': first_receive,
            }
        }

    @staticmethod
    def dumps(data):
        return json.dumps(data)


class ChatConsumer(AsyncWebsocketConsumer):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.chat_id = None
        self.room_group_name = None

    async def connect(self):
        self.chat_id = self.scope['url_route']['kwargs']['chat_id']
        self.room_group_name = 'chat_%s' % self.chat_id

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

        ids = ChatService.get_ids(self.chat_id)

        msgs = await ChatService.get_chat_messages(ids)
        data = ChatService.create_message_meta(msgs, True)
        data = ChatService.dumps(data)

        await self.send(text_data=data)

    async def disconnect(self, code):
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    async def receive(self, text_data=None, bytes_data=None):
        if text_data:
            text_data_json = json.loads(text_data)
            message = text_data_json['message']

            ids = ChatService.get_ids(self.chat_id)

            msgs = await ChatService.save_message(ids, message)
            data = ChatService.create_message_meta(msgs, False)
            data = ChatService.dumps(data)

            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'chat_message',
                    'data': data
                }
            )

    async def chat_message(self, event):
        data = event['data']

        await self.send(text_data=data)
