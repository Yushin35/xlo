
from django.conf.urls import url, include
from django.urls import path

from . import views

urlpatterns = [
    path('accounts/', include('django.contrib.auth.urls')),
    url(r'^$', views.index, name='index'),
    url(
        r'^(?P<user_pk>[^/]+)/(?P<advert_pk>[^/]+)/$',
        views.chat,
        name='chat',
    ),
]
