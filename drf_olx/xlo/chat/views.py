import json

from django.shortcuts import render
from django.utils.safestring import mark_safe
from django.conf import settings
from django.contrib.auth.decorators import login_required

from hashids import Hashids


@login_required(login_url='accounts/login/')
def index(request):
    return render(request, 'chat/index.html', {})


@login_required(login_url='accounts/login/')
def chat(request, user_pk, advert_pk):

    hashids = Hashids(settings.HASHID_SALT)
    (user_pk, ) = hashids.decode(user_pk)
    (advert_pk, ) = hashids.decode(advert_pk)
    chat_id = hashids.encode(request.user.id, user_pk, advert_pk)

    return render(request, 'chat/chat.html', {
        'chat_id': mark_safe(json.dumps(chat_id)),
    })
