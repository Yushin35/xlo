from django.contrib import admin
from .models import User


@admin.register(User)
class ProductAdmin(admin.ModelAdmin):
    view_on_site = True
    list_display = ('pk', 'username', )
