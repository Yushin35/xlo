from django.db import models
from django.contrib.auth.models import AbstractUser


from xlo.adverts.models import Favorite, Advert


class User(AbstractUser):
    favorites = models.ManyToManyField(Advert, through=Favorite)
    mobile_phone = models.CharField(
        max_length=255,
        null=True,
        blank=False,
        unique=True,
    )
