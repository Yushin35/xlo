from django.conf.urls import include, url

from rest_framework_nested import routers
from rest_framework_swagger.views import get_swagger_view

from .views import (
    AdvertViewSet,
    AdvertImageUploadViewSet,
    FavoriteAdvertView,
    FacebookLogin,
)


schema_view = get_swagger_view(title='Pastebin API')  # pylint: disable=invalid-name


advert_router = routers.SimpleRouter()  # pylint: disable=invalid-name
advert_router.register(r'adverts', AdvertViewSet, basename='adverts')
advert_router.register(r'favorites', FavoriteAdvertView, basename='favorites')

urlpatterns = [
    url(r'^', include(advert_router.urls)),
    url(r'^$', schema_view),
    url(
        r'^upload_image/(?P<advert_pk>[0-9]+)$',
        AdvertImageUploadViewSet.as_view(),
    )
]

urlpatterns += [
    url(r'^rest-auth/facebook/$', FacebookLogin.as_view(), name='fb_login'),
]
