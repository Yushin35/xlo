from django.conf import settings

from rest_framework.serializers import ModelSerializer
from rest_framework import serializers

from hashids import Hashids

from xlo.users.models import User
from .models import (
    Advert,
    Category,
    SubCategory,
    Image,
    Favorite,
    Message,
)


class SubCategorySerializer(ModelSerializer):
    class Meta:
        model = SubCategory
        fields = ('pk', 'name')


class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = ('pk', 'name')


class HashIdField(serializers.Field):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.hashids = Hashids(settings.HASHID_SALT)

    def to_internal_value(self, data):
        (pk, ) = self.hashids.decode(data)

        return pk

    def to_representation(self, value):
        return self.hashids.encode(value)


class UserSerializer(ModelSerializer):
    pk = HashIdField(read_only=True, required=False)

    class Meta:
        model = User
        fields = ('pk', 'username')
        write_only_fields = ('password', )
        read_only_fields = ('pk', )


class AdvertSerializer(ModelSerializer):
    category = CategorySerializer(read_only=True, required=False)
    owner = UserSerializer(read_only=True, required=False)
    sub_category = SubCategorySerializer(read_only=True, required=False)

    category_id = serializers.IntegerField(required=True, write_only=True)
    sub_category_id = serializers.IntegerField(required=True, write_only=True)
    owner_id = HashIdField(read_only=True, required=False)
    pk = HashIdField(read_only=True, required=False)

    class Meta:
        model = Advert
        fields = (
            'id', 'pk', 'owner', 'name',
            'price', 'text', 'agreed',
            'category', 'sub_category',
            'category_id', 'sub_category_id',
            'owner_id',
        )
        read_only_fields = ('activity', )


class ImageSerializer(ModelSerializer):
    advert = AdvertSerializer(read_only=True, required=False)
    advert_id = serializers.IntegerField(required=True, write_only=True)

    class Meta:
        model = Image
        fields = (
            'pk',
            'pic',
            'advert',
            'advert_id',
        )


class FavoriteSerializer(ModelSerializer):
    user = UserSerializer(read_only=True, required=False)
    advert = AdvertSerializer(read_only=True, required=False)

    advert_id = serializers.IntegerField()

    class Meta:
        model = Favorite
        fields = ('pk', 'user', 'advert',
                  'advert_id', 'user_id',)
        write_only = ('advert_id',)
        read_only_fields = ('user_id', )


class MessageSerializer(ModelSerializer):
    class Meta:
        model = Message
        fields = ('message', 'datetime')
