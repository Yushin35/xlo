from django.contrib import admin
from .models import (
    Image,
    Advert,
    Category,
    SubCategory,
    Message,
)


admin.site.register(Image, admin.ModelAdmin)
admin.site.register(Advert, admin.ModelAdmin)
admin.site.register(Category, admin.ModelAdmin)
admin.site.register(SubCategory, admin.ModelAdmin)
admin.site.register(Message, admin.ModelAdmin)
