from django.db import models
from django.utils import timezone
from django.conf import settings


class Category(models.Model):
    name = models.CharField(max_length=255, unique=True)


class SubCategory(models.Model):
    category = models.ForeignKey(
        Category,
        on_delete=models.CASCADE,
        related_name='sub_categories',
    )
    name = models.CharField(max_length=255, unique=True)


class Advert(models.Model):
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='adverts',
        null=False
    )
    name = models.CharField(max_length=255, null=False)
    text = models.TextField(max_length=5000, null=False)
    price = models.DecimalField(max_digits=12, decimal_places=2, null=False)
    category = models.ForeignKey(
        Category,
        null=False,
        blank=False,
        on_delete=models.CASCADE,
    )
    sub_category = models.ForeignKey(
        SubCategory,
        null=False,
        blank=False,
        on_delete=models.CASCADE,
        related_name='adverts',
    )
    agreed = models.BooleanField(default=False)
    activity = models.DateTimeField(default=timezone.now(), null=False)


class Image(models.Model):
    advert = models.ForeignKey(
        Advert,
        null=False,
        on_delete=models.CASCADE,
        related_name='images',
    )
    pic = models.ImageField(null=False, upload_to='adverts')


class Favorite(models.Model):

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    advert = models.ForeignKey(Advert, on_delete=models.CASCADE)

    class Meta:
        unique_together = (("user", "advert"),)


class Message(models.Model):
    message = models.CharField(max_length=255, null=False)
    datetime = models.DateTimeField(null=False, default=timezone.now())
    advert = models.ForeignKey(Advert,
                               null=False,
                               on_delete=models.CASCADE,
                               related_name='messages')
    receiver = models.ForeignKey(settings.AUTH_USER_MODEL,
                                 on_delete=models.CASCADE,
                                 related_name='+')
    sender = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE,
                               related_name='+')
