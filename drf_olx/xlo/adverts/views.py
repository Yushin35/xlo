from django.db.utils import IntegrityError


from rest_framework import viewsets, views
from rest_framework import status
from rest_framework.authentication import BasicAuthentication
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response
from rest_auth.registration.views import SocialLoginView

from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter


from .models import (
    Advert,
    Favorite,
)
from .serializers import (
    AdvertSerializer,
    ImageSerializer,
    FavoriteSerializer,
)
from .permissions import (
    IsAuthenticatedOrReadOnly,
    IsAdminOrOwnerOrReadOnly,
)
from .exceptions import BadRequest
from .paginations import StandardResultsSetPagination
from .filters_backend import (
    PriceFilterBackend,
    ActivityFilterBackend,
    CategoryFilterBackend,
)


class AdvertViewSet(viewsets.ModelViewSet):
    authentication_classes = (BasicAuthentication, JSONWebTokenAuthentication)
    permission_classes = (IsAuthenticatedOrReadOnly, IsAdminOrOwnerOrReadOnly)
    serializer_class = AdvertSerializer
    queryset = Advert.objects.all()
    filter_backends = (
        PriceFilterBackend,
        CategoryFilterBackend,
        ActivityFilterBackend,
    )
    pagination_class = StandardResultsSetPagination

    def perform_create(self, serializer):
        serializer.validated_data['owner_id'] = self.request.user.id
        serializer.is_valid(raise_exception=True)

        try:
            serializer.save()
        except IntegrityError:
            raise BadRequest


class AdvertImageUploadViewSet(views.APIView):
    parser_classes = (FileUploadParser, )

    def post(self, request, advert_pk):
        serializers = ImageSerializer(data={
            'advert_id': advert_pk,
            'pic': request.data['file'],
        })

        serializers.is_valid(raise_exception=True)
        try:
            serializers.save()
        except IntegrityError:
            raise BadRequest

        return Response(serializers.data, status=status.HTTP_201_CREATED)


class FavoriteAdvertView(viewsets.ModelViewSet):
    permission_classes = (IsAdminOrOwnerOrReadOnly,)
    queryset = Favorite.objects.all()
    serializer_class = FavoriteSerializer
    pagination_class = StandardResultsSetPagination

    def perform_create(self, serializer):

        serializer.validated_data['user_id'] = self.request.user.id
        try:
            serializer.save()
        except IntegrityError:
            raise BadRequest


class FacebookLogin(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter
