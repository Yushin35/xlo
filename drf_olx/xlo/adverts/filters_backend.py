from datetime import timedelta

from rest_framework import filters

from django.utils import timezone


class PriceFilterBackend(filters.BaseFilterBackend):

    def filter_queryset(self, request, queryset, view):
        price_start = request.query_params.get('price_start')
        price_end = request.query_params.get('price_end')
        if price_start:
            queryset = queryset.filter(price__gte=price_start)
        if price_end:
            queryset = queryset.filter(price__lte=price_end)

        return queryset


class CategoryFilterBackend(filters.BaseFilterBackend):

    def filter_queryset(self, request, queryset, view):
        category = request.query_params.get('category')
        if category:
            queryset = queryset.filter(category__name=category)

        return queryset


class ActivityFilterBackend(filters.BaseFilterBackend):
    ACTIVITY_DAY = 5
    ACTIVITY_HOUR = 0
    ACTIVITY_MINUTE = 3
    ACTIVITY_SECOND = 1

    def filter_queryset(self, request, queryset, view):
        return queryset.filter(
            activity__gt=timezone.now() - timedelta(
                days=self.ACTIVITY_DAY,
                hours=self.ACTIVITY_HOUR,
                minutes=self.ACTIVITY_MINUTE,
                seconds=self.ACTIVITY_SECOND,
            )
        )
