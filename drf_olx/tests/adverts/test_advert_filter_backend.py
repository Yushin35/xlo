from pytest import fixture

from xlo.adverts.models import (
    Category,
    SubCategory,
    Advert,
)
from xlo.users.models import User
from xlo.adverts.filters_backend import (
    PriceFilterBackend,
    CategoryFilterBackend,
    ActivityFilterBackend
)


@fixture
def user(db):
    return User.objects.create(username='user1', password='123')


@fixture
def categories(db):
    return [
            Category.objects.create(
                name='c_%s' % i,
            )
            for i in range(5)
        ]


@fixture
def sub_categories(db, categories):
    return [
        SubCategory.objects.create(name='sb_%s' % i, category=categories[i])
        for i in range(5)
    ]


@fixture
def adverts(db, user, sub_categories):
    return [
        Advert.objects.create(
                owner=user,
                name='a_%s' % i,
                price=i * 10.0,
                text='text',
                category=sub_categories[i].category,
                sub_category=sub_categories[i],
            )
        for i in range(5)
    ]


class MockRequest:
    def __init__(self, price_start, price_end, category=None):
        self.query_params = {
            'price_start': price_start,
            'price_end': price_end,
            'category': category,
        }


class TestFilterBackend:
    def test_price_filter_backend(self, adverts):

        request_se = MockRequest(20, 40)
        request_s = MockRequest(20, None)
        request_e = MockRequest(None, 40)
        request_all = MockRequest(None, None)

        price_filter = PriceFilterBackend()

        assert [
                   adverts[2].pk,
                   adverts[3].pk,
                   adverts[4].pk
               ] == sorted([
            advert.pk
            for advert in price_filter.filter_queryset(request_se, Advert.objects.filter(), None)
        ])
        assert [
                   adverts[2].pk,
                   adverts[3].pk,
                   adverts[4].pk
               ] == sorted([
            advert.pk
            for advert in price_filter.filter_queryset(request_s, Advert.objects.filter(), None)
        ])
        assert sorted([advert.pk for advert in adverts]) == sorted([
            advert.pk
            for advert in price_filter.filter_queryset(request_e, Advert.objects.filter(), None)
        ])

        assert sorted([advert.pk for advert in adverts]) == sorted([
            advert.pk
            for advert in price_filter.filter_queryset(request_all, Advert.objects.filter(), None)
        ])

    def test_category_filter_backend(self, adverts):
        request_category = MockRequest(None, None, 'c_1')
        request_category_none = MockRequest(None, None)

        category_filter = CategoryFilterBackend()

        assert [adverts[1].pk, ] == sorted([
            advert.pk
            for advert in category_filter.filter_queryset(request_category, Advert.objects.filter(), None)
        ])
        assert sorted([advert.pk for advert in adverts]) == sorted([
            advert.pk
            for advert in category_filter.filter_queryset(request_category_none, Advert.objects.filter(), None)
        ])

    def test_activity_filter_backend(self, adverts):
        request = MockRequest(None, None)

        activity_filter = ActivityFilterBackend()

        assert sorted([advert.pk for advert in adverts]) == sorted([
            advert.pk
            for advert in activity_filter.filter_queryset(request, Advert.objects.filter(), None)
        ])
