from rest_framework.test import APIClient
from pytest import fixture
from xlo.adverts.models import (
    Advert,
    Category,
    SubCategory,
    Image,
    Favorite,
    Message,
)



from xlo.adverts.models import (
    Category,
    SubCategory,
    Advert,
)
from xlo.users.models import User


@fixture
def user(db):
    return User.objects.create(username='user1', password='123')


@fixture
def categories(db):
    return [
            Category.objects.create(
                name='c_%s' % i,
            )
            for i in range(5)
        ]


@fixture
def sub_categories(db, categories):
    return [
        SubCategory.objects.create(name='sb_%s' % i, category=categories[i])
        for i in range(5)
    ]


@fixture
def adverts(db, user, sub_categories):
    return [
        Advert.objects.create(
                owner=user,
                name='a_%s' % i,
                price=i * 10.0,
                text='text',
                category=sub_categories[i].category,
                sub_category=sub_categories[i],
            )
        for i in range(5)
    ]


class TestAdvertImageUploadViewSet:
    def test_create_ok(self, adverts, user):
        client = APIClient()

        client.force_authenticate(user=user)

        response = client.post(
            '/upload_image/%s' % adverts[0].pk,
            open('/home/user/work/projects/drf_sale/drf_olx/tests/adverts/pic.jpg', 'rb').read(),
            content_type='image/jpeg',
            HTTP_CONTENT_DISPOSITION='attachment; filename="img.jpg"',
        )

        assert response.status_code == 201

    def test_create_err(self, user):
        client = APIClient()

        client.force_authenticate(user=user)

        response = client.post(
            '/upload_image/0',
            open('/home/user/work/projects/drf_sale/drf_olx/tests/adverts/pic.jpg', 'rb').read(),
            content_type='image/jpeg',
            headers={
                'Content-Disposition': 'attachment; filename="img.jpg"',
            },

        )

        assert response.status_code == 400


class TestAdvertViewSet:
    def test_perform_create_ok(self, sub_categories, adverts, user):
        client = APIClient()

        client.force_authenticate(user=user)

        response = client.post('/adverts/', {
                    "name": "phone",
                    "price": "6123.04",
                    "text": "t4",
                    "agreed": False,
                    "category_id": adverts[0].category.id,
                    "sub_category_id": adverts[0].sub_category.id,
                }, format='json')

        assert response.status_code == 201

    def test_perform_create_err(self, adverts, user):
        client = APIClient()

        client.force_authenticate(user=user)

        response = client.post('/adverts/', {
                    "name": "phone",
                    "price": "6123.04",
                    "text": "t4",
                    "agreed": False,
                    "category_id": None,
                    "sub_category_id": None
                }, format='json')

        assert response.status_code == 400
