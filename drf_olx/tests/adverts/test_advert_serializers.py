from unittest.mock import MagicMock
from unittest.mock import patch

from hashids import Hashids


from xlo.adverts.serializers import (
    HashIdField,
)


class TestHashIdField:

    @patch('xlo.adverts.serializers.settings')
    def test_to_representation(self, settings):

        salt = '^%$#KH123%^HV#%@#hgvhsd1243124'
        settings.HASHID_SALT = salt

        hashids = Hashids(salt)
        representation = hashids.encode(1)

        field = HashIdField()

        assert field.to_representation(1) == representation

    @patch('xlo.adverts.serializers.settings')
    def test_to_internal_value(self, settings):
        salt = '^%$#KH123%^HV#%@#hgvhsd1243124'
        settings.HASHID_SALT = salt

        hashids = Hashids(salt)
        representation = hashids.encode(1)
        (pk, ) = hashids.decode(representation)

        field = HashIdField()

        assert field.to_representation(1) == representation
        assert field.to_internal_value(field.to_representation(1)) == pk
