import pytest

from unittest.mock import patch
from pytest import fixture

from hashids import Hashids

from xlo.chat.consumers import ChatService

from xlo.adverts.models import (
    Category,
    SubCategory,
    Advert,
)
from xlo.users.models import User

from xlo.adverts.models import Message

@fixture
def user(db):
    return User.objects.create(username='user1', password='123')


@fixture
def categories(db):
    return [
            Category.objects.create(
                name='c_%s' % i,
            )
            for i in range(5)
        ]


@fixture
def sub_categories(db, categories):
    return [
        SubCategory.objects.create(name='sb_%s' % i, category=categories[i])
        for i in range(5)
    ]


@fixture
@pytest.mark.asyncio
def adverts(db, user, sub_categories):
    return [
        Advert.objects.create(
                owner=user,
                name='a_%s' % i,
                price=i * 10.0,
                text='text',
                category=sub_categories[i].category,
                sub_category=sub_categories[i],
            )
        for i in range(5)
    ]


class TestChatService:
    @patch('xlo.chat.consumers.settings')
    def test_get_ids(self, settings):
        salt = '^%$#KH123%^HV#%@#hgvhsd1243124'
        settings.HASHID_SALT = salt

        hashids = Hashids(salt)
        representation = hashids.encode(1, 2, 3)

        assert {
                   'receiver_id': 1,
                   'sender_id': 2,
                   'advert_id': 3,
               } == ChatService.get_ids(representation)

    @pytest.mark.asyncio
    @pytest.mark.django_db
    async def test_save_message(self, adverts, user):
        salt = '^%$#KH123%^HV#%@#hgvhsd1243124'
        hashids = Hashids(salt)
        representation = hashids.encode(user.pk, adverts[0].owner.pk, adverts[0].pk)

        ids = ChatService.get_ids(representation, hash_salt=salt)

        messages = Message.objects.create(**ids, message='hello')
        messages.save()

        res = await ChatService.save_message(ids, 'hello')

        assert '' == res
