from rest_framework.test import APIClient
from pytest import fixture

from xlo.adverts.models import (
    Category,
    SubCategory,
    Advert,
)
from xlo.users.models import User


@fixture
def user(db):
    return User.objects.create(username='user1', password='123')


@fixture
def categories(db):
    return [
            Category.objects.create(
                name='c_%s' % i,
            )
            for i in range(5)
        ]


@fixture
def sub_categories(db, categories):
    return [
        SubCategory.objects.create(name='sb_%s' % i, category=categories[i])
        for i in range(5)
    ]


@fixture
def adverts(db, user, sub_categories):
    return [
        Advert.objects.create(
                owner=user,
                name='a_%s' % i,
                price=i * 10.0,
                text='text',
                category=sub_categories[i].category,
                sub_category=sub_categories[i],
            )
        for i in range(5)
    ]


class TestIsAuthenticatedOrReadOnly:
    def test_has_object_permission_ok(self, sub_categories, categories, adverts, user):
        client = APIClient()

        get_response_all = client.get('/adverts/')
        get_response_one = client.get('/adverts/%s/' % adverts[0].pk)
        post_response = client.post('/adverts/', {
            "name": "phone",
            "price": "6123.04",
            "text": "t4",
            "agreed": False,
            "category_id": categories[0].id,
            "sub_category_id": sub_categories[0].id
        }, format='json')
        put_response = client.put('/adverts/%s/' % adverts[0].pk, {
            "name": "phone",
            "price": "6123.04",
            "text": "t4",
            "agreed": False,
            "category_id": categories[0].id,
            "sub_category_id": sub_categories[0].id
        }, format='json')
        patch_response = client.patch('/adverts/%s/' % adverts[0].pk, {
            "price": "6123.04",
        }, format='json')
        delete_response = client.delete('/adverts/%s/' % adverts[0].pk, {}, format='json')

        assert get_response_all.status_code == 200
        assert get_response_one.status_code == 200
        assert post_response.status_code == 401
        assert put_response.status_code == 401
        assert patch_response.status_code == 401
        assert delete_response.status_code == 401

    def test_has_object_permission_err(self, sub_categories, categories, adverts, user):
        client = APIClient()
        client.force_authenticate(user=user)

        get_response_all = client.get('/adverts/')
        get_response_one = client.get('/adverts/%s/' % adverts[0].pk)
        post_response = client.post('/adverts/', {
            "name": "phone",
            "price": "6123.04",
            "text": "t4",
            "agreed": False,
            "category_id": categories[0].id,
            "sub_category_id": sub_categories[0].id
        }, format='json')
        put_response = client.put('/adverts/%s/' % adverts[0].pk, {
            "name": "phone",
            "price": "6123.04",
            "text": "t4",
            "agreed": False,
            "category_id": categories[0].id,
            "sub_category_id": sub_categories[0].id
        }, format='json')
        patch_response = client.patch('/adverts/%s/' % adverts[0].pk, {
            "price": "10000.04",
        }, format='json')
        delete_response = client.delete('/adverts/%s/' % adverts[1].pk, {}, format='json')

        bad_request_post_response = client.post('/adverts/', {
            "name": "phone",
            "price": "6123.04",
            "agreed": False,
            "category_id": categories[0].id,
            "sub_category_id": sub_categories[4].id
        }, format='json')

        assert get_response_all.status_code == 200
        assert get_response_one.status_code == 200
        assert post_response.status_code == 201
        assert put_response.status_code == 200
        assert patch_response.status_code == 200
        assert delete_response.status_code == 204
        assert bad_request_post_response.status_code == 400
