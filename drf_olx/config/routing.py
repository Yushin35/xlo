from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
import xlo.chat.routing

application = ProtocolTypeRouter({
    # (http->django views is added by default)
    'websocket': AuthMiddlewareStack(

        URLRouter(
            xlo.chat.routing.websocket_urlpatterns
        )
    ),
})
