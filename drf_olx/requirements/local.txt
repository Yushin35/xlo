-r ./base.txt

-r ./base.txt

Werkzeug==0.15.1  # https://github.com/pallets/werkzeug
ipdb==0.12  # https://github.com/gotcha/ipdb
dj-database-url
psycopg2-binary

# Testing
# ------------------------------------------------------------------------------
pytest==4.3.1  # https://github.com/pytest-dev/pytest
pytest-sugar==0.9.2  # https://github.com/Frozenball/pytest-sugar

# Code quality
# ------------------------------------------------------------------------------
flake8==3.7.5  # https://github.com/PyCQA/flake8
coverage==4.5.3  # https://github.com/nedbat/coveragepy
black==19.3b0  # https://github.com/ambv/black

# Django
# ------------------------------------------------------------------------------
django-extensions==2.1.6  # https://github.com/django-extensions/django-extensions
django-coverage-plugin==1.6.0  # https://github.com/nedbat/django_coverage_plugin
pytest-django==3.4.8  # https://github.com/pytest-dev/pytest-django

django-getenv
django-dotenv
ipython

pytest
pylint
pylint-django
flake8
pytest-cov
pytest-asyncio