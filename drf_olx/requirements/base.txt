# Django
# ------------------------------------------------------------------------------
django==2.0.13  # pyup: < 2.1  # https://www.djangoproject.com/
django-mptt

# Django REST Framework
djangorestframework==3.9.2  # https://github.com/encode/django-rest-framework
djangorestframework-jwt
django-rest-swagger
drf-nested-routers
django-rest-auth[with_social]
channels
hashids
Pillow==5.4.1  # https://github.com/python-pillow/Pillow
dj_database_url
